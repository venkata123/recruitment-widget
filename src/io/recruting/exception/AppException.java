package io.recruting.exception;

public class AppException extends Exception {

	private static final long serialVersionUID = 1L;

	public AppException(String message) {
		super(message);
		
	}
}
