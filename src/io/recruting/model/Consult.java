package io.recruting.model;

public class Consult {
	
	private String id;
	private String firstName;
	private String lastName;
	private String date;
	private int calls;
	private String recrut1;
	private int sub1;
	private String report1;
	private String recrut2;
	private int sub2;
	private String report2;
	private String recrut3;
	private int sub3;
	private String report3;
	private String recrut4;
	private int sub4;
	private String report4;
	private String supplement;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getCalls() {
		return calls;
	}
	public void setCalls(int calls) {
		this.calls = calls;
	}
	public String getRecrut1() {
		return recrut1;
	}
	public void setRecrut1(String recrut1) {
		this.recrut1 = recrut1;
	}
	public int getSub1() {
		return sub1;
	}
	public void setSub1(int sub1) {
		this.sub1 = sub1;
	}
	public String getReport1() {
		return report1;
	}
	public void setReport1(String report1) {
		this.report1 = report1;
	}
	public String getRecrut2() {
		return recrut2;
	}
	public void setRecrut2(String recrut2) {
		this.recrut2 = recrut2;
	}
	public int getSub2() {
		return sub2;
	}
	public void setSub2(int sub2) {
		this.sub2 = sub2;
	}
	public String getReport2() {
		return report2;
	}
	public void setReport2(String report2) {
		this.report2 = report2;
	}
	public String getRecrut3() {
		return recrut3;
	}
	public void setRecrut3(String recrut3) {
		this.recrut3 = recrut3;
	}
	public int getSub3() {
		return sub3;
	}
	public void setSub3(int sub3) {
		this.sub3 = sub3;
	}
	public String getReport3() {
		return report3;
	}
	public void setReport3(String report3) {
		this.report3 = report3;
	}
	public String getRecrut4() {
		return recrut4;
	}
	public void setRecrut4(String recrut4) {
		this.recrut4 = recrut4;
	}
	public int getSub4() {
		return sub4;
	}
	public void setSub4(int sub4) {
		this.sub4 = sub4;
	}
	public String getReport4() {
		return report4;
	}
	public void setReport4(String report4) {
		this.report4 = report4;
	}
	public String getSupplement() {
		return supplement;
	}
	public void setSupplement(String supplement) {
		this.supplement = supplement;
	}
	
}
