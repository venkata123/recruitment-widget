package io.recruting.dao;

import io.recruting.model.Consult;

import io.recruting.exception.AppException;

import io.recruiting.util.DBUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.NotFoundException;

public class ConsultDAO {

		public List<Consult> findall() throws AppException {
			List<Consult> consults = new ArrayList<Consult>();
			Connection con = DBUtil.connect();
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			try {
				ps = con.prepareStatement("SELECT FIRST_NAME,DAT,CALLS,SUPPLEMENT FROM user");
				rs = ps.executeQuery();
				
				while(rs.next()) {
					Consult cst = new Consult();
					cst.setFirstName(rs.getString("FIRST_NAME"));
					cst.setDate(rs.getString("DAT"));
					cst.setCalls(rs.getInt("CALLS"));
					cst.setSupplement(rs.getString("SUPPLEMENT"));
					
					consults.add(cst);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new AppException(e.getMessage());
			}
			finally {
				
				try {
					if (ps != null) {
						ps.close();
					}

					if (rs != null) {
						rs.close();
					}

					if (con != null) {
						con.close();
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			
			return consults;
	}

		public Consult create(Consult cst) throws AppException{
			Connection con = DBUtil.connect();
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			try {
				ps = con.prepareStatement("INSERT INTO user (FIRST_NAME, LAST_NAME, DAT, CALLS,RECRUITER1,SUB1,REPORT1,RECRUITER2,SUB2,REPORT2,RECRUITER3,SUB3,REPORT3,RECRUITER4,SUB4,REPORT4, SUPPLEMENT) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, cst.getFirstName());
				ps.setString(2, cst.getLastName());
				ps.setString(3, cst.getDate());
				ps.setInt(4, cst.getCalls());
				ps.setString(5, cst.getRecrut1());
				ps.setInt(6, cst.getSub1());
				ps.setString(7, cst.getReport1());
				ps.setString(8, cst.getRecrut2());
				ps.setInt(9, cst.getSub2());
				ps.setString(10, cst.getReport2());
				ps.setString(11, cst.getRecrut3());
				ps.setInt(12, cst.getSub3());
				ps.setString(13, cst.getReport3());
				ps.setString(14, cst.getRecrut4());
				ps.setInt(15, cst.getSub4());
				ps.setString(16, cst.getReport4());
				
				ps.setString(17, cst.getSupplement());
				
				ps.executeUpdate();
				
				rs = ps.getGeneratedKeys();
				
				if(rs.next()) {
					cst.setId(rs.getString(1));
					
					}
				else{
					throw new NotFoundException("The id not found");
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new AppException(e.getMessage());
			}
			finally {
				
				try {
					if (ps != null) {
						ps.close();
					}

					if (rs != null) {
						rs.close();
					}

					if (con != null) {
						con.close();
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
				
			return cst;
		}

		public List<Consult> finddate(String date) throws AppException {
			List<Consult> consults = new ArrayList<Consult>();
			
			Connection con = DBUtil.connect();
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			try {
				ps = con.prepareStatement("SELECT * FROM user where DAT=?");
				ps.setString(1, date);
			
				rs = ps.executeQuery();
				
				while(rs.next()) {
					Consult cst = new Consult();
					cst.setId(rs.getString("id"));
					cst.setFirstName(rs.getString("FIRST_NAME"));
					cst.setLastName(rs.getString("LAST_NAME"));
					cst.setCalls(rs.getInt("CALLS"));
					cst.setDate(rs.getString("DAT"));
					cst.setRecrut1(rs.getString("RECRUITER1"));
					cst.setSub1(rs.getInt("SUB1"));
					cst.setReport1(rs.getString("REPORT1"));
					cst.setRecrut2(rs.getString("RECRUITER2"));
					cst.setSub2(rs.getInt("SUB2"));
					cst.setReport2(rs.getString("REPORT2"));
					cst.setRecrut3(rs.getString("RECRUITER3"));
					cst.setSub3(rs.getInt("SUB3"));
					cst.setReport3(rs.getString("REPORT3"));
					cst.setSupplement(rs.getString("SUPPLEMENT"));
					consults.add(cst);
					
					}
				
			} catch (SQLException e) {
				e.printStackTrace();
				throw new AppException(e.getMessage());
			}
			finally {
				
				try {
					if (ps != null) {
						ps.close();
					}

					if (rs != null) {
						rs.close();
					}

					if (con != null) {
						con.close();
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
				
			return consults;

		}

		public Consult findreport(String date, String name) throws AppException {
			Consult cst = null;
			Connection con = DBUtil.connect();
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			try {
				ps = con.prepareStatement("SELECT * FROM user where DAT=? and FIRST_NAME=?");
				ps.setString(1, date);
				ps.setString(2, name);
				rs = ps.executeQuery();
				
				if(rs.next()) {
					cst = new Consult();
					cst.setId(rs.getString("id"));
					cst.setFirstName(rs.getString("FIRST_NAME"));
					cst.setLastName(rs.getString("LAST_NAME"));
					cst.setCalls(rs.getInt("CALLS"));
					cst.setDate(rs.getString("DAT"));
					cst.setRecrut1(rs.getString("RECRUITER1"));
					cst.setSub1(rs.getInt("SUB1"));
					cst.setReport1(rs.getString("REPORT1"));
					cst.setRecrut2(rs.getString("RECRUITER2"));
					cst.setSub2(rs.getInt("SUB2"));
					cst.setReport2(rs.getString("REPORT2"));
					cst.setRecrut3(rs.getString("RECRUITER3"));
					cst.setSub3(rs.getInt("SUB3"));
					cst.setReport3(rs.getString("REPORT3"));
					cst.setSupplement(rs.getString("SUPPLEMENT"));
					
					}
				else{
					throw new NotFoundException("The name not found");
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new AppException(e.getMessage());
			}
			finally {
				
				try {
					if (ps != null) {
						ps.close();
					}

					if (rs != null) {
						rs.close();
					}

					if (con != null) {
						con.close();
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
				
			return cst;

		}

		public List<Consult> findname(String name) throws AppException {
			List<Consult> consults = new ArrayList<Consult>();
			
			Connection con = DBUtil.connect();
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			try {
				ps = con.prepareStatement("SELECT * FROM user where FIRST_NAME=?");
				ps.setString(1, name);
			
				rs = ps.executeQuery();
				
				while(rs.next()) {
					Consult cst = new Consult();
					cst.setId(rs.getString("id"));
					cst.setFirstName(rs.getString("FIRST_NAME"));
					cst.setLastName(rs.getString("LAST_NAME"));
					cst.setCalls(rs.getInt("CALLS"));
					cst.setDate(rs.getString("DAT"));
					cst.setRecrut1(rs.getString("RECRUITER1"));
					cst.setSub1(rs.getInt("SUB1"));
					cst.setReport1(rs.getString("REPORT1"));
					cst.setRecrut2(rs.getString("RECRUITER2"));
					cst.setSub2(rs.getInt("SUB2"));
					cst.setReport2(rs.getString("REPORT2"));
					cst.setRecrut3(rs.getString("RECRUITER3"));
					cst.setSub3(rs.getInt("SUB3"));
					cst.setReport3(rs.getString("REPORT3"));
					cst.setSupplement(rs.getString("SUPPLEMENT"));
					consults.add(cst);
					
					}
				
			} catch (SQLException e) {
				e.printStackTrace();
				throw new AppException(e.getMessage());
			}
			finally {
				
				try {
					if (ps != null) {
						ps.close();
					}

					if (rs != null) {
						rs.close();
					}

					if (con != null) {
						con.close();
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
				
			return consults;

		}




}
