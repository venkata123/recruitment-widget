package io.recruting.controller;

import io.recruting.dao.ConsultDAO;
import io.recruting.model.Consult;

import io.recruting.exception.AppException;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


@Path("/consult")
public class ConsultController {
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List <Consult> findall(){
		
		List <Consult> consults = null;
		System.out.println("I am at findall");
		
		try {
			ConsultDAO cdao = new ConsultDAO();
			consults = cdao.findall();
		} catch (AppException e) {
			e.printStackTrace();
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		return consults;
	}	
	
	@GET
	@Path("/{date}")
	@Produces(MediaType.APPLICATION_JSON)
	public List <Consult>  finddate(@PathParam("date") String date){
		List <Consult> consults = null;
		
		System.out.println("I am at findwith date");
		System.out.println(date);
		try {
			ConsultDAO cdao = new ConsultDAO();
			consults = cdao.finddate(date);
		} catch (AppException e) {
			e.printStackTrace();
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		return consults;
		
	}
	@GET
	@Path("byname/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public List <Consult>  findname(@PathParam("name") String name){
		List <Consult> consults = null;
		
		System.out.println("I am at findwith name");
		System.out.println(name);
		try {
			ConsultDAO cdao = new ConsultDAO();
			consults = cdao.findname(name);
		} catch (AppException e) {
			e.printStackTrace();
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		return consults;
		
	}
	
	@GET
	@Path("/{date}/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Consult findreport(@PathParam("date") String date, @PathParam("name") String name){
		Consult ct = null;
		System.out.println("I am at findreport");
		System.out.println(date);
		System.out.println(name);
		try {
			ConsultDAO cdao = new ConsultDAO();
			ct = cdao.findreport(date,name);
		} catch (AppException e) {
			e.printStackTrace();
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		return ct;
		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Consult create(Consult cst){
		
		System.out.println("I am at create");
		
		try {
			ConsultDAO cdao = new ConsultDAO();
			cst = cdao.create(cst);
		} catch (AppException e) {
			e.printStackTrace();
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		return cst;
	}
}
