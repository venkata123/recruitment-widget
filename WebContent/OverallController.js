/**
 * Created by sairam on 3/11/2016.
 */
(function(){

    'use strict';

    angular
        .module('recruting')
        .controller('OverallController', OverallController);

    OverallController.$inject = ['dataService'];

    function OverallController(dataService) {
        var overallVm = this;

        overallVm.report = [];

        dataService
            .getreport()
            .then(function(data) {
                overallVm.report = data;
            }, function(error) {
                console.log(error);
            });

        console.log('CustomerListController');
    }

})();
