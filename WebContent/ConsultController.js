/**
 * Created by sairam on 3/10/2016.
 */
(function() {
    'use strict';

    angular
        .module('recruting')
        .controller('ConsultController', ConsultController)
    ConsultController.$inject = ['dataService', '$window']
    function ConsultController(dataService, $window) {
        var consultVm = this;
        consultVm.report = {};

        consultVm.addReport = function (resObj) {
            dataService
                .putreport(resObj)
                .then(function (response) {
                    console.log(response.id);
                    $window.alert("your id is:" + response.id + "Report Submitted Sussccessfully Thank you")
                });

            console.log("ResCreate up and running!");
        }
    }

})();