/**
 * Created by sairam on 3/11/2016.
 */
(function(){
    'use strict';

    angular
        .module('recruting', ['ngRoute'])
        .config(moduleConfig);

    moduleConfig.$inject = ['$routeProvider'];
    function moduleConfig ($routeProvider) {

        $routeProvider
            .when('/owner', {
                templateUrl: 'overall.tmpl.html',
                controller: 'OverallController',
                controllerAs: 'overallVm'

            })
            .when('/bydate', {
                templateUrl: 'date.tmpl.html',
                controller: 'DateController',
                controllerAs: 'dateVm'

            })
            .when('/datereport/:id', {
                templateUrl: 'datereport.tmpl.html',
                controller: 'DateReportController',
                controllerAs: 'daterVm'

            })
            .when('/dayreport/:id/:name', {
                templateUrl: 'dayreport.tmpl.html',
                controller: 'DayReportController',
                controllerAs: 'dayVm'

            })
            .when('/overall/:name', {
                templateUrl: 'overallreport.tmpl.html',
                controller: 'OverallReportController',
                controllerAs: 'overallrVm'

            })

            .otherwise({
                redirectTo: '/owner'
            })
    }
})();