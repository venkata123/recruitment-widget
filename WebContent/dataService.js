/**
 * Created by sairam on 3/10/2016.
 */
(function() {
    'use strict';

    angular
        .module('recruting')
        .service('dataService', dataService);

    dataService.$inject = ['$http', '$q'];

    function dataService ($http, $q) {

        var self = this;

        self.putreport = function (report) {
            var defer = $q.defer();

            $http
                .post(' http://10.1.10.252:8080/Recruting/app/consult', report)
                .then(function (response) {
                    defer.resolve(response.data);
                }, function (error) {
                    defer.reject(error.status);
                });
            return defer.promise;
        };

        self.getreport = function () {
            var defer = $q.defer();

            $http
                .get('http://10.1.10.252:8080/Recruting/app/consult')
                .then(function (response){
                    defer.resolve(response.data);
                }, function (error){
                    defer.reject(error.status);
                });
            return defer.promise;
        };
        self.getconsults = function (id) {
            var defer = $q.defer();
            $http
                .get('http://10.1.10.252:8080/Recruting/app/consult/' + id)
                .then(function (response) {
                    defer.resolve(response.data);
                }, function (error) {
                    defer.reject(error.status);
                });
            return defer.promise;
        };
        self.getconsultreport = function (id,name) {
            var defer = $q.defer();
            $http
                .get('http://10.1.10.252:8080/Recruting/app/consult/' + id + '/' + name)
                .then(function (response) {
                    defer.resolve(response.data);
                }, function (error) {
                    defer.reject(error.status);
                });
            return defer.promise;
        };
        self.getoverallreport = function (name) {
            var defer = $q.defer();
            $http
                .get('http://10.1.10.252:8080/Recruting/app/consult/byname/'  + name)
                .then(function (response) {
                    defer.resolve(response.data);
                }, function (error) {
                    defer.reject(error.status);
                });
            return defer.promise;
        };


    }



})();