/**
 * Created by sairam on 3/10/2016.
 */
(function(){
    'use strict';

    angular
        .module('recruting', ['ngRoute'])
        .config(moduleConfig);

    moduleConfig.$inject = ['$routeProvider'];
    function moduleConfig ($routeProvider) {

        $routeProvider
            .when('/guest', {
                templateUrl: 'user.tmpl.html',
                controller: 'ConsultController',
                controllerAs: 'consultVm'

            })
            .when('/owner', {
                templateUrl: 'ownerlogin.tmpl.html',
                controller: 'OwnerLoginController',
                controllerAs: 'loginVm'
            })


            .otherwise({
                redirectTo: '/guest'
            })
    }
})();